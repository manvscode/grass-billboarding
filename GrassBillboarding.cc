/*
 *   GrassBillboarding.cc
 *
 *   Grass billboarding example implemented for FIU's SIG-Graph community.
 *   Coded by Joseph A. Marrero, October 20, 2006
 */

#include "GrassBillboarding.h" // needed to avoid exit() problem
#include <gl/glut.h>
#include <cassert>
#include <ctime>
#include <algorithm>

#include "Math/Matrix4x4.h"
using namespace std;

#define TEST	testSphere( )

int main( int argc, char *argv[] )
{
	glutInit( &argc, argv );
	glutInitDisplayMode( GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH );
	glutInitWindowSize( 800, 600 );
	
	glutCreateWindow( "Grass Billboarding Demo" );

	glutFullScreen( );

	initialize( );
	glutDisplayFunc( render );
	glutReshapeFunc( resize );
	glutKeyboardFunc( keyboard_keypress );
	glutIdleFunc( rotateScene );
	
	glutMainLoop( );
	return 0;
}

void initialize( )
{
	ImageIO::initialize( );
	int ret = ImageIO::loadTGAFile( "grass.tga", &grassTargaFile);
	assert( ret == 1 );

	camera.x = 0.0f;
	camera.y = 0.0f;
	camera.z = 0.0f;
	camera.yAngle = 0.0f;
	glEnable( GL_ALPHA_TEST );
	
	glEnable( GL_POINT_SMOOTH );
	glEnable( GL_LINE_SMOOTH );
	glEnable( GL_POLYGON_SMOOTH );
	glHint( GL_POINT_SMOOTH_HINT, GL_FASTEST );
	glHint( GL_LINE_SMOOTH_HINT, GL_FASTEST );
	glHint( GL_POLYGON_SMOOTH_HINT, GL_FASTEST );
	glHint( GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST );
	
	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	

	glShadeModel( GL_SMOOTH );
	glClearDepth( 1.0f );						
	glDepthFunc( GL_ALWAYS );
	glEnable( GL_DEPTH_TEST );
	

	glPixelStorei( GL_PACK_ALIGNMENT, 4 );
	glPixelStorei( GL_UNPACK_ALIGNMENT, 4 );



	glEnable( GL_TEXTURE_2D );
	glBindTexture( GL_TEXTURE_2D, grass );


	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP );
	glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP );
	assert( !glGetError( ) );
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, grassTargaFile.imageWidth, grassTargaFile.imageHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, grassTargaFile.imageData );

	glClearColor( 0.0f, 0.0f, 0.0f, 0.0f );

	//generate grass positions...
	srand( (unsigned int) time(NULL) );

	for( unsigned int i = 0; i < MAX; i++ )
	{
		grasses[ i ].x = ((rand( ) % 1000 - 500) / 1000.0f ) * FLOOR_DIM * 1.9 + ((rand() % 100) / 1000.0f);
		grasses[ i ].y = 0.0f;
		grasses[ i ].z = ((rand( ) % 1000 - 500) / 1000.0f ) * FLOOR_DIM * 1.9 + ((rand() % 100) / 1000.0f);
	}
	
	// ensure painter's algorithm
	sort( &grasses[ 0 ], &grasses[ MAX - 1 ] );

	camera.y = 3;
}

void deinitialize( )
{
	// free image texture memory...
	ImageIO::destroyTGAImageData( &grassTargaFile );
}


void render( )
{
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	glLoadIdentity( );

	camera.x = RADIUS * cos( camera.yAngle );
	camera.z = RADIUS * sin( camera.yAngle );
	gluLookAt( camera.x, camera.y, camera.z, 0, 0, 0, 0, 1, 0 );

	if( camera.yAngle >= 360.0f ) camera.yAngle = 0.0f;
	camera.yAngle += 0.005f;


	glDisable( GL_TEXTURE_2D );
	
	// rotating base...
	glColor3f( 0.79f, 0.77f, 0.35f );
	glBegin( GL_QUADS );
		glVertex3f( -FLOOR_DIM, 0.0f, -FLOOR_DIM );
		glVertex3f( FLOOR_DIM, 0.0f, -FLOOR_DIM );
		glVertex3f( FLOOR_DIM, 0.0f, FLOOR_DIM );
		glVertex3f( -FLOOR_DIM, 0.0f, FLOOR_DIM );
	glEnd( );
	
	// billboarding...
	static GLfloat matrix[ 16 ] = {0}; // array initialization trick.
	Vector3<float> look( camera.x - 0, camera.y - 0, camera.z - 0 );
	Vector3<float> right( 1, 0, 0 );
	Vector3<float> up( 0, 1, 0 );

	right = crossProduct( look, up );
	up = crossProduct( right, look );

	look.normalize( );
	right.normalize( );
	up.normalize( );

	assert( dotProduct( look, up ) == dotProduct( up, right ) == dotProduct( look, right ) == 0 );

	// right
	matrix[ 0 ] = right.getX( );
	matrix[ 1 ] = right.getY( );
	matrix[ 2 ] = right.getZ( );
	matrix[ 3 ] = 0.0f;

	// up
	matrix[ 4 ] = up.getX( );
	matrix[ 5 ] = up.getY( );
	matrix[ 6 ] = up.getZ( );
	matrix[ 7 ] = 0.0f;

	// look
	matrix[ 8 ] = look.getX( );
	matrix[ 9 ] = look.getY( );
	matrix[ 10 ] = look.getZ( );
	matrix[ 11 ] = 0.0f;
	
	matrix[ 12 ] = matrix[ 13 ] = matrix[ 14 ] = 0.0f; //remove the position
	matrix[ 15 ] = 1.0f;

	glEnable( GL_TEXTURE_2D );


	//draw alot of grass
	for( int i = 0; i < MAX; i++ )
	{	
		glPushMatrix( );
		glTranslatef( grasses[ i ].x, 0, grasses[ i ].z );
		glMultMatrixf( matrix );
		
		
		renderGrassBlade( );
		glPopMatrix( );
	}
	
	TEST;

	int width = glutGet((GLenum)GLUT_WINDOW_WIDTH);
	int height = glutGet((GLenum)GLUT_WINDOW_HEIGHT);

	writeText( GLUT_BITMAP_HELVETICA_18, std::string("Grass Billboarding"), 2, 22 );
	writeText( GLUT_BITMAP_8_BY_13, std::string("Press Q to quit."), 2, 5 );
	glutSwapBuffers( );
}

void renderGrassBlade( )
{
	glPushMatrix( );
		
	// grass...
	glBindTexture( GL_TEXTURE_2D, grass );
	glBegin( GL_QUADS );
		
		glColor3f( 0.8f, 0.8f, 0.8f );
		glTexCoord2i( 0, 0 );
		glVertex3f( -0.05f, 0.0f, 0.0f );
		glTexCoord2i( 1, 0 );
		glVertex3f( 0.05f, 0.0f, 0.0f );
		glTexCoord2i( 1, 1 );
		glVertex3f( 0.05f, 0.55f, 0.0f );
		glTexCoord2i( 0, 1 );
		glVertex3f( -0.05f, 0.55f, 0.0f );
	glEnd( );
	glBindTexture( GL_TEXTURE_2D, 0 );

	glPopMatrix( );
}

void testSphere( )
{
	//draw sphere...
	glPushMatrix( );
		GLUquadric *q = gluNewQuadric( );
		glColor3f( 1.0f, 1.0f, 1.0f );
		glTranslatef( 0.0f, 1.0f, 0.0f );
		gluSphere( q, 0.05, 5, 5 );
		gluDeleteQuadric( q );
		renderCoordinateAxes( 0, 0, 0 );
	glPopMatrix( );
}

void renderCoordinateAxes( float x, float y, float z )
{
	glPushMatrix( );
	glDisable( GL_BLEND );

	
	glTranslatef( x, y, z );

	glBegin( GL_LINES );
		// x
		glColor3f( 1.0f, 0.0f, 0.0f );
		glVertex3i( 0, 0, 0);
		glVertex3i( 1, 0, 0);
		// y
		glColor3f( 0.0f, 1.0f, 0.0f );
		glVertex3i( 0, 0, 0);
		glVertex3i( 0, 1, 0);
		// z
		glColor3f( 0.0f, 0.0f, 1.0f );
		glVertex3i( 0, 0, 0);
		glVertex3i( 0, 0, 1);
	glEnd( );

	glEnable( GL_BLEND );
	glPopMatrix( );
}

void resize( int width, int height )
{
	glViewport( 0, 0, width, height );
	glMatrixMode( GL_PROJECTION );
	glLoadIdentity( );
	

	if( height == 0 )
		height = 1;
		
	gluPerspective( 40.0f, (GLfloat) width / (GLfloat) height, 0.1f, 1000.0f );
	glMatrixMode( GL_MODELVIEW );

}

void keyboard_keypress( unsigned char key, int x, int y )
{
	switch( key )
	{
		case 'Q':
		case 'q':
		case GLUT_KEY_F1:
		case GLUT_KEY_END:
			deinitialize( );
			exit( 0 );
			break;
		default:
			break;
	}

}

void writeText( void *font, std::string &text, int x, int y )
{
	int width = glutGet((GLenum)GLUT_WINDOW_WIDTH);
	int height = glutGet((GLenum)GLUT_WINDOW_HEIGHT);

	glDisable( GL_DEPTH_TEST );
	glDisable(GL_TEXTURE_2D);


	glMatrixMode( GL_PROJECTION );
	glPushMatrix( );
		glLoadIdentity( );	
		glOrtho( 0, width, 0, height, 1.0, 10.0 );
			
		glMatrixMode( GL_MODELVIEW );
		glPushMatrix( );
			glLoadIdentity( );
			glColor3f( 1.0f, 1.0f, 1.0f );
			glTranslatef( 0.0f, 0.0f, -1.0f );
			glRasterPos2i( x, y );

			for( int i = 0; i < text.size( ); i++ )
				glutBitmapCharacter( font, text[ i ] );
			
		glPopMatrix( );
		glMatrixMode( GL_PROJECTION );
	glPopMatrix( );
	glMatrixMode( GL_MODELVIEW );


	glEnable( GL_DEPTH_TEST );
	glEnable( GL_TEXTURE_2D );
	
}

void rotateScene( )
{
	glutPostRedisplay( );
}