#ifndef _GRASSBILLBOARDING_H_
#define _GRASSBILLBOARDING_H_
/*
 *   GrassBillboarding.h
 *
 *   Grass billboarding example implemented for FIU's SIG-Graph community.
 *   Coded by Joseph A. Marrero, October 20, 2006
 */
#include <string>
#include "ImageIOLibrary/ImageIO.h"
#include "Math/Vector3.h"

typedef struct tPosition {
	float x;
	float y;
	float z;
	float yAngle;

	bool operator<( struct tPosition &p )
	{
		if( z < p.z )
			return true;
		else if( z == p.z && y < p.y )
			return true;
		else if(  z == p.z && y == p.y && x < p.x )
			return true;
		return false;
	}

} Position;

#define RADIUS		10.0f
#define MAX			3000
#define FLOOR_DIM	3

void initialize( );
void deinitialize( );

void render( );
void renderGrassBlade( );
void resize( int width, int height );
void keyboard_keypress( unsigned char key, int x, int y );
void writeText( void *font, std::string &text, int x, int y );
void rotateScene( );
void testSphere( );
void renderCoordinateAxes( float x, float y, float z );



ImageIO::TGAFILE grassTargaFile;
unsigned int grass;
Position camera;
Position grasses[ MAX ];


float light_position[] = { 3.0f, 5.0f, 3.0f, 0.0f };
float light_ambient[] = { 0.3f, 0.3f,  0.3f, 0.0f };


#endif