#pragma once
/*		Vector3.h
 *
 *		Vectors for 3D space.
 *		Coded by Joseph A. Marrero
 *		5/7/04
 */

#include <ostream>

template <class AnyType = float>
class Vector3
{

  private:
	union {
		struct{
			AnyType x, y, z;
		};
		AnyType data[ 3 ];
	};

  public:


	explicit Vector3( void );
	explicit Vector3( const AnyType x, const AnyType y, const AnyType z );
	virtual ~Vector3( void );

	// Get components 
	const AnyType getX( ) const;
	const AnyType getY( ) const;
	const AnyType getZ( ) const;
	void setX( const AnyType X );
	void setY( const AnyType Y );
	void setZ( const AnyType Z );
	// Vector Operations
	const Vector3 &operator =( const Vector3 &vector );
	const Vector3 &operator +=( const Vector3 &vector );
	const Vector3 &operator -=( const Vector3 &vector );
	AnyType dotProduct( const Vector3 &vector );		// Dot Product
	Vector3 crossProduct( const Vector3 &vector );	// Cross Product
	AnyType getMagnitude( ) const;							// Get Vector Magnitude
	void normalize( );								// Normalize Vector
	const bool isNormalized( ) const;
	void negate( );									// Negate Vector

	//friend ostream& operator <<(ostream&, const Vector3&);

	static const Vector3 X_VECTOR;
	static const Vector3 Y_VECTOR;
	static const Vector3 Z_VECTOR;
};


template <class AnyType>
Vector3<AnyType> operator +( const Vector3<AnyType> &v1, const Vector3<AnyType> &v2 );				// Vector Sum
template <class AnyType>
Vector3<AnyType> operator -( const Vector3<AnyType> &v1, const Vector3<AnyType> &v2 );				// Vector Difference
template <class AnyType>
Vector3<AnyType> operator *( const Vector3<AnyType> &vector, const AnyType scalar );			// Vector-Scalar Multiplication
template <class AnyType>
Vector3<AnyType> operator *( const Vector3<AnyType> scalar, const Vector3<AnyType> &vector );			// Vector-Scalar Multiplication
template <class AnyType>
Vector3<AnyType> operator /( const Vector3<AnyType> &vector, const AnyType scalar );			// Vector- Scalar Division
template <class AnyType>
Vector3<AnyType> operator /( const Vector3<AnyType> &vector, const AnyType scalar );			// Vector- Scalar Division

template <class AnyType>
AnyType dotProduct( const Vector3<AnyType> &v1, const Vector3<AnyType> &v2 );	
template <class AnyType>
AnyType operator *( const Vector3<AnyType> &v1, const Vector3<AnyType> &v2 );		

template <class AnyType>
Vector3<AnyType> crossProduct( const Vector3<AnyType> &v1, const Vector3<AnyType> &v2 );		
//template <class AnyType>
//Vector3<AnyType> operator><( const Vector3<AnyType> &v1, const Vector3<AnyType> &v2 );


template <class AnyType>
AnyType angleBetween( const Vector3<AnyType> &v1, const Vector3<AnyType> &v2 );
#include "Vector3.inl"