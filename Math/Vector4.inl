/*		CVector4.cpp
 *
 *		Vectors for 3D space.
 *		Coded by Joseph A. Marrero
 *		5/7/04
 */
#include <cmath>
#include "Vector4.h"

template <class AnyType>
CVector4<AnyType>::CVector4( )
{
	x = y = z = w = 0.0;

}

template <class AnyType>
CVector4<AnyType>::CVector4( const AnyType x, const AnyType y, const AnyType z, const AnyType w )
{
	this->x = x;
	this->y = y;
	this->z = z;
	this->w = w;
}

template <class AnyType>
CVector4<AnyType>::~CVector4( )
{
}

template <class AnyType>
const AnyType CVector4<AnyType>::getX( ) const
{
	return x;
}

template <class AnyType>
const AnyType CVector4<AnyType>::getY( ) const
{
	return y;
}

template <class AnyType>
const AnyType CVector4<AnyType>::getZ( ) const
{
	return z;
}

template <class AnyType>
const AnyType CVector4<AnyType>::getW( ) const
{
	return w;
}

template <class AnyType>
void CVector4<AnyType>::setX( const AnyType X )
{ x = X; }

template <class AnyType>
void CVector4<AnyType>::setY( const AnyType Y )
{ y = Y; }

template <class AnyType>
void CVector4<AnyType>::setZ( const AnyType Z )
{ z = Z; }

template <class AnyType>
void CVector4<AnyType>::setW( const AnyType W )
{ w = W; }

template <class AnyType>
const CVector4<AnyType> &CVector4<AnyType>::operator =(const CVector4<AnyType> &vector)
{
	if( this != &vector )
	{
		x = vector.getX( );
		y = vector.getY( );
		z = vector.getZ( );
		w = vector.getW( );
	}

	return *this;
}

template <class AnyType>
const CVector4<AnyType> &CVector4<AnyType>::operator +=(const CVector4<AnyType> &vector)
{
	if( this != &vector )
	{
		x = x + vector.getX( );
		y = y + vector.getY( );
		z = z + vector.getZ( );
		w = w + vector.getW( );
	}

	return *this;
}

template <class AnyType>
const CVector4<AnyType> &CVector4<AnyType>::operator -=(const CVector4<AnyType> &vector)
{
	if( this != &vector )
	{
		x = x - vector.getX( );
		y = y - vector.getY( );
		z = z - vector.getZ( );
		w = w - vector.getW( );
        
	}
	
	return *this;
}

template <class AnyType>
AnyType CVector4<AnyType>::dotProduct(const CVector4<AnyType> &vector)
{
	return (x * vector.x + y * vector.y + z * vector.z + w * vector.w);
}

template <class AnyType>
CVector4<AnyType> CVector4<AnyType>::crossProduct(const CVector4<AnyType> &vector)
{
	CVector4<AnyType> Result;

	Result.x = y*vector.z - z*vector.y;
	Result.y = z*vector.x - x*vector.z;
	Result.z = x*vector.y - y*vector.x;

	return Result;
}
//ostream& operator << (ostream& out, const CVector4<AnyType> &vector)
//{
//	out << "(" << vector.x << ", " << vector.y << ", " << vector.z << ")";
//
//	return out;
//}
template <class AnyType>
AnyType CVector4<AnyType>::getMagnitude( ) const
{
	return sqrt( x*x + y*y + z*z );
}

template <class AnyType>
void CVector4<AnyType>::normalize( )
{
	AnyType m = getMagnitude( );
	x = x / m;
	y = y / m;
	z = z / m;
	//*this/getMagnitude();
}

template <class AnyType>
const bool CVector4<AnyType>::isNormalized( ) const
{ return ( x == 1 && y == 1 && z == 1 ); }

template <class AnyType>
void CVector4<AnyType>::negate()
{
	x = -x;
	y = -y;
	z = -z;
}


// static common vectors
template <class AnyType>
const CVector4<AnyType> CVector4<AnyType>::X_VECTOR( static_cast<AnyType>(1),  static_cast<AnyType>(0), static_cast<AnyType>(0) );

template <class AnyType>
const CVector4<AnyType> CVector4<AnyType>::Y_VECTOR( static_cast<AnyType>(0),  static_cast<AnyType>(1), static_cast<AnyType>(0) );

template <class AnyType>
const CVector4<AnyType> CVector4<AnyType>::Z_VECTOR( static_cast<AnyType>(0), static_cast<AnyType>(0),  static_cast<AnyType>(1) );


template <class AnyType>
CVector4<AnyType> operator +(const CVector4<AnyType> &v1, const CVector4<AnyType> &v2)				// Vector Sum
{
	CVector4<AnyType> Result;
	
	Result.setX( v1.getX( ) + v2.getX( ) );
	Result.setY( v1.getY( ) + v2.getY( ) );
	Result.setZ( v1.getZ( ) + v2.getZ( ) );

	return Result;
}
template <class AnyType>
CVector4<AnyType> operator -(const CVector4<AnyType> &v1, const CVector4<AnyType> &v2)				// Vector Difference
{
	CVector4<AnyType> Result;
	
	Result.setX( v1.getX( ) - v2.getX( ) );
	Result.setY( v1.getY( ) - v2.getY( ) );
	Result.setZ( v1.getZ( ) - v2.getZ( ) );

	return Result;
}
template <class AnyType>
CVector4<AnyType> operator *(const CVector4<AnyType> &vector, const AnyType scalar)			// Vector-Scalar Multiplication
{
	CVector4<AnyType> Result;

	Result.setX( scalar * vector.getX( ) );
	Result.setY( scalar * vector.getY( ) );
	Result.setZ( scalar * vector.getZ( ) );

	return Result;
}
template <class AnyType>
CVector4<AnyType> operator *( const AnyType scalar, const CVector4<AnyType> &vector)			// Vector-Scalar Multiplication
{
	CVector4<AnyType> Result;

	Result.setX( scalar * vector.getX( ) );
	Result.setY( scalar * vector.getY( ) );
	Result.setZ( scalar * vector.getZ( ) );

	return Result;
}
template <class AnyType>
CVector4<AnyType> operator /(const CVector4<AnyType> &vector, const AnyType scalar)			// Vector- Scalar Division
{
	CVector4<AnyType> Result;

	Result.setX( vector.getX( ) / scalar );
	Result.setY( vector.getY( ) / scalar );
	Result.setZ( vector.getZ( ) / scalar );

	return Result;
}
template <class AnyType>
CVector4<AnyType> operator /(const AnyType scalar, const CVector4<AnyType> &vector)			// Vector- Scalar Division
{
	CVector4<AnyType> Result;

	Result.setX( scalar / vector.getX( ) );
	Result.setY( scalar / vector.getY( ) );
	Result.setZ( scalar / vector.getZ( ) );

	return Result;
}


template <class AnyType>
AnyType dotProduct( const CVector4<AnyType> &v1, const CVector4<AnyType> &v2 )
{
	return ( v1.getX( ) * v2.getX( ) + v1.getY( ) * v2.getY( ) + v1.getZ( ) * v2.getZ( ) );
}
template <class AnyType>
AnyType operator *( const CVector4<AnyType> &v1, const CVector4<AnyType> &v2 )
{
	return DotProduct( v1, v2 );
}
template <class AnyType>
CVector4<AnyType> crossProduct( const CVector4<AnyType> &v1, const CVector4<AnyType> &v2 )
{
	CVector4<AnyType> Result;

	Result.setX( v1.getY( ) * v2.getZ( ) - v1.getZ( ) * v2.getY( ) );
	Result.setY( v1.getZ( ) * v2.getX( ) - v1.getX( ) * v2.getZ( ) );
	Result.setZ( v1.getX( ) * v2.getY( ) - v1.getY( ) * v2.getX( ) );
	
	return Result;
}
//template <class AnyType>
//CVector4<AnyType> operator( const CVector4<AnyType> &v1, const CVector4<AnyType> &v2 )
//{
//	return CrossProduct( v1, v2 );
//}


template <class AnyType>
AnyType angleBetween( const CVector4<AnyType> &v1, const CVector4<AnyType> &v2 ) // in radians
{
	float dp = dotProduct( v1, v2 );
	float v1M = v1.getMagnitude( ),
		  v2M = v2.getMagnitude( );

	return ( (AnyType) acos( dp / ( v1M * v2M ) ) );
}